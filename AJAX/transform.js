'use strict';

// Task Transforming Module, Version 1.0.0
// Creates an HTML file from a Tasks JSON file

// Imports
const fs = require('fs');

// Constants
const templatePath = './template.html';

// Functions

var openCategory = function (category = 'Category') {
	if(typeof(category) === 'undefined' || category.length === 0) {
		category = 'Category';
	}

	var html = '<div class="category">\n'+
				'\t\t\t\t<h2>' + category + '</h2>\n'+
				'\t\t\t\t<table>\n';

	return html;
};

var closeCategory = function () {
	return '\t\t\t\t</table>\n'+
			'\n'+
			'\t\t\t\t<hr />\n'+
			'\t\t\t</div>';
	};

var addItem = function (item) {
	if(typeof(item) === 'undefined' || item.length === 0) {
		return '';
};

	var html = '';

/*
	if(item.length > 2) {
		switch(item[1]) {
			case 1:
				html +=
'\t\t\t\t<tr class="bug">\n'+
'\t\t\t\t\t<td>';
				break;
			case 2:
				html +=
'\t\t\t\t<tr class="idea">\n'+
'\t\t\t\t\t<td>';
				break;
			default:
				html +=
'\t\t\t\t<tr>\n'+
'\t\t\t\t\t<td>';
		}
	} else {
		html +=
'\t\t\t\t<tr>\n'+
'\t\t\t\t\t<td>';
	}
*/

	var color = '';

	if(item.length > 2) {
		color = ' class="';
		switch(item[1]) {
			case 1:
				color += 'gray';
				break;
			case 2:
				color += 'black';
				break;
			case 3:
				color += 'red';
				break;
			case 4:
				color += 'orange';
				break;
			case 5:
				color += 'yellow';
				break;
			case 6:
				color += 'green';
				break;
			case 7:
				color += 'cyan';
				break;
			case 8:
				color += 'blue';
				break;
			case 9:
				color +='purple';
				break;
			case 10:
				color += 'magenta';
				break;
			case 11:
				color += 'brown';
				break;
			//default: // White (no color)
		}
		color += '"';
	}


	html += '\t\t\t\t\t<tr' + color + '>\n' + '\t\t\t\t\t\t<td>';

	if(item.length > 1) {
		switch(item[0]) {
			case 1: // 25%
				html += '&#9684;';
				break;
			case 2: // 50%
				html += '&#9681;';
				break;
			case 3: // 75%
				html += '&#9685;';
				break;
			case 4: // 100%
				html += '&#9679;';
				break;
			case 5: // Optional
				html += '&#9676;';
				break;
			default: // 0%
				html += '&#9675;';
				break;
		}
		html += '</td>\n' + '\t\t\t\t\t\t<td>';	
	} else {
		html += '&#9675;</td>\n' + '\t\t\t\t\t\t<td>';
	}

	html += item[item.length - 1];

	html += '</td>\n' + '\t\t\t\t\t</tr>\n';

	return html;
}

var toHTML = function (tasks, full = true, minify = true) {
	let html = '';
	// Transform the Tasks into HTML
	if(typeof(tasks) !== 'undefined' && tasks.length !== 0) {
		let categories = Object.keys(tasks);
		for(let i = 0; i < categories.length; ++i) {
			let category = categories[i];
			html += openCategory(category);

			for(let j = 0; j < tasks[category].length; ++j) {
				let item = tasks[category][j];
				html += addItem(item);
			}

			html += closeCategory();
		}
	}

	// Load the Template
	let template = '';
	if(full === true) {
		template = fs.readFileSync(templatePath,{'encoding': 'utf-8'});

		// Replace the BODY with the Categories and Items
		template = template.replace(/§BODY§/, html);
	} else {
		template = html;
	}

	// Remove Comments (Single-line and Multi-line)
	template = template.replace(/\/\/.*|\/\*.*\*\/|\<\!\-\-.*\-\-\>/g, '');

	// Minify (remove Tabs and Newlines)
	if(minify === true) {
		// HTML
		template = template.replace(/\>[\s\n]+\</mg, '><');
		//template = template.replace(/\>[\s\n]+/mg, '>');
		//template = template.replace(/[\s\n]+\</mg, '<');

		// JS / CSS
		template = template.replace(/\;\s+/mg, ';');
		template = template.replace(/,[\s\n]+/mg, ',');
		//template = template.replace(/:[\s\n]+/mg, ':'); // Interferes with HTML
		template = template.replace(/[\s\n]*\{[\s\n]*/mg, '{');
		template = template.replace(/[\s\n]*\}[\s\n]*/mg, '}');
	}

	//console.log(template);
	return template;
}

module.exports.toHTML = toHTML;
