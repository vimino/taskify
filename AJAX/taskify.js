'use strict';

const App = {
	'title': 'Taskify',
	'version': '1.0.0',
	'license': 'Copyright (c) 2017, Vítor T. Martins' +
		'\n' +
		'The source code is under the MIT license:' +
		'\n' +
		'Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:' +
		'\n' +
		'The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.' +
		'\n' +
		'THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.'
};

// http://luke.breuer.com/tutorial/javascript-context-menu-tutorial.htm
// http://www.ibm.com/developerworks/library/wa-aj-ajaxhistory/

// Libraries
const fs = require('fs');
const server = require('server');
const TransformTasks = require('transform');

// Constants
const input = './tasks.json';
const output = './index.html';
const port = 8000;
let html;

function readJSON(path) {
	// console.log('> File => JSON');

	// Set the input File Path
	let file = '';
	if(typeof(path) === 'undefined') {
		file = input;
	}

	// Read the File
	let json = '{}';
	try {
		if(fs.existsSync(file)) {
			json = fs.readFileSync(file);
		}
	} catch(x) {
		console.log('Warning @ readJSON: ' + x);
	}

	// Parse the JSON
	let tasks = {};
	try {
		tasks = JSON.parse(json);
	} catch(x) {
		console.log('Error @ readJSON: ' + x);
		return {};
	}

	return tasks;
}

function parseJSON(json, toObject = true) {
	// console.log('> String => JSON');
	let tasks = {};

	// Parse the JSON
	try {
		tasks = JSON.parse(json);
		if(toObject === true && typeof(tasks) === 'string') {
			tasks = JSON.parse(tasks);
		}
	} catch(x) {
		console.log('Error @ parseJSON: ' + x);
		return {};
	}

	return tasks;
}

function saveJSON(json, path) {
	// console.log('> JSON => File');

	// Set the output File Path
	let file = '';
	if(typeof(path) === 'undefined') {
		file = input;
	}

	// Write the JSON
	try {
		fs.writeFileSync(file, json);
	} catch(x) {
		console.log('Error @ saveJSON: ' + x);
	}
}

function buildHTML(tasks) {
	// console.log('> Tasks => HTML');

	// Transform JSON into HTML then return
	return TransformTasks.toHTML(tasks); // , true, false);
}

function updateTasks(data) {
	console.log('< JSON <~Ajax~ Client');

	// Parse the updated JSON
	var tasks = parseJSON(data.json);

	// Rebuild the HTML from JSON
	html = buildHTML(tasks);

	// Update the JSON file
	saveJSON(parseJSON(data.json, false));
}

function serveHTML(tasks) {
	// console.log('> Serve HTML (on Port ' + port + ')');

	if(typeof(tasks) === 'undefined') {
		console.log('> Read JSON');
		let json = readJSON();
		console.log('> JSON => HTML');
		html = buildHTML(json);
	} else {
		html = buildHTML(tasks);
	}

	console.log('> Serve HTML');
	// Serve the HTML and set up a POST Task Updater
	let { get, post } = server.router;
	server({ 'port': port },
		get('/', (request, response) => response.send(html)),
		post('/', (request, response) => updateTasks(request.body))
	);
}

// Patterns

const rxHelp = new RegExp('^-{0,2}h(elp)?$');
const rxVersion = new RegExp('^-{0,2}v(ersion)?$');
const rxExport = new RegExp('^-{0,2}e(xport)?$');

// Execution

var args = process.argv.slice(2);
if(args.length === 0) {
	serveHTML();
}

for(let i = 0; i < args.length; ++i) {
	let arg = args[i];

	if(rxHelp.test(arg)) {
		console.log('Usage: ' + App.title.toLowerCase() + ' [OPTION]... [FILE]...');
		console.log('By default a "tasks.json" is loaded/saved.');
		console.log();
		console.log('  -e, --export    Exports the (index.)HTML created from the JSON');
		console.log('  -h, --help      Shows this information and exits');
		console.log('  -v, --version   Shows the Applications version');
		return;
	} else if(rxVersion.test(arg)) {
		console.log(App.title + ' ' + App.version);
		console.log(App.license);
	} else if(rxExport.test(arg)) {
		var target = output;

		if(args[i+1]) {
			target = args[i+1];
			i += 1;
		}

		console.log('> Export to "' + target + '"');
		try {
			let tasks = readJSON();
			let html = buildHTML(tasks);
			fs.writeFileSync(target, html);
		} catch(x) {
			console.log('Exception @ Export: ' + x);
		}
	} else {
		console.log('> Unknown command "' + arg + '"');
	}
}
