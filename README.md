## Taskify
### A toy Javascript Task manager

I've been using unicode characters to make pretty HTML lists and decided it was time I made it into a project.
On January 1st, 2017 I found a guide about using *AJAX*. This got me curious so I decided to make a simple server and communicate with it.

Around April 20th, 2018 I decided to try using the new *LocalStorage* and *IndexedDB* functionalities.
This would make Taskify work without a server (but that still has its own use-cases).

Note that *LocalStorage* works like local cookies so blocking them will likely stop it from working.
As for the *IndexedDB*, a single entry is used (the JSON for all the tasks) and that's not the best way to use a Database.

Also added Import and Export features since it already used functions to save and load from JSON files.

### Guides

- [AJAX](https://www.ibm.com/developerworks/library/wa-aj-ajaxhistory/wa-aj-ajaxhistory-pdf.pdf)
- [Javascript Context Menus](http://luke.breuer.com/tutorial/javascript-context-menu-tutorial.htm)
- [LocalStorage](https://developer.mozilla.org/en-US/docs/Web/API/Storage/LocalStorage)
- [IndexedDB](https://developer.mozilla.org/en-US/docs/Web/API/IndexedDB_API)
- [Saving Files](https://stackoverflow.com/questions/13405129/javascript-create-and-save-file)
- [Reading Files](https://www.html5rocks.com/en/tutorials/file/dndfiles/#toc-reading-files)

### License

Copyright &copy; 2017-2018, VIMinO <https://www.vimino.net/>

The source code is licensed under the [MIT License (MIT)](https://opensource.org/licenses/MIT).
